﻿Public Class Form1
    Dim clearSiguiente As Boolean = False
    Dim calculoGuardado1 As Double
    Dim calculoGuardado2 As Double
    Dim resultado As Double
    Dim operacionGuardada As String
    Dim comaAnyadida As Boolean = False

    Private Sub Button_Seleccion_Click(sender As Object, e As EventArgs) Handles Button0.Click,
        Button1.Click, Button2.Click, Button3.Click, Button4.Click, Button5.Click, Button6.Click,
        Button7.Click, Button8.Click, Button9.Click, ButtonCambioSigno.Click, ButtonDec.Click, ButtonIgual.Click,
        ButtonDiv.Click, ButtonMult.Click, ButtonMas.Click, ButtonSqrt.Click, ButtonC.Click, ButtonMenos.Click

        'Recogemos el click de cualquier botón y le pasamos los datos a la siguiente función:
        Enviar_datos_display(sender.name, sender.text)

    End Sub

    Private Sub Enviar_datos_display(boton As String, texto As String)

        Select Case boton
            Case "ButtonCambioSigno"
                If Display.Text.Length > 0 Then
                    Dim numSigno As Double = 0

                    numSigno = Convert.ToDouble(Display.Text) * -1
                    calculoGuardado1 = Convert.ToDouble(numSigno)
                    Display.Text = calculoGuardado1

                End If

            Case "ButtonDec"
                If comaAnyadida = False Then
                    Display.Text += ","
                    comaAnyadida = True
                End If

            Case "ButtonIgual"
                'Cuando le damos a guardar, comprobará que operación hemos seleccionado anteriormente y hará el cálculo
                Select Case operacionGuardada

                    Case "ButtonIgual"
                        End

                    Case "div"

                        Try
                            calculoGuardado2 = Convert.ToDouble(Display.Text)
                            resultado = calculoGuardado1 / calculoGuardado2
                            Display.Text = resultado
                            calculoGuardado1 = Convert.ToDouble(Display.Text)

                        Catch dbz As DivideByZeroException
                            MsgBox("¿Por qué alguien dividiria entre cero?")
                        End Try

                    Case "mult"

                        calculoGuardado2 = Convert.ToDouble(Display.Text)

                        resultado = calculoGuardado1 * calculoGuardado2
                        Display.Text = resultado
                        calculoGuardado1 = Convert.ToDouble(Display.Text)

                    Case "mas"

                        calculoGuardado2 = Convert.ToDouble(Display.Text)

                        resultado = calculoGuardado1 + calculoGuardado2
                        Display.Text = resultado
                        calculoGuardado1 = resultado
                        calculoGuardado1 = Convert.ToDouble(Display.Text)

                    Case "menos"

                        calculoGuardado2 = Convert.ToDouble(Display.Text)
                        resultado = calculoGuardado1 - calculoGuardado2
                        Display.Text = resultado

                        calculoGuardado1 = Convert.ToDouble(Display.Text)

                End Select

            Case "ButtonDiv"
                operacionGuardada = "div"
                clearSiguiente = True

            Case "ButtonMult"
                operacionGuardada = "mult"
                clearSiguiente = True

            Case "ButtonMas"

                operacionGuardada = "mas"
                clearSiguiente = True
                MsgBox(calculoGuardado1)

            Case "ButtonSqrt"
                clearSiguiente = True
                Display.Text = Math.Sqrt(calculoGuardado1)
                calculoGuardado1 = Math.Sqrt(calculoGuardado1)

            Case "ButtonC"
                clearSiguiente = False
                calculoGuardado1 = 0
                calculoGuardado2 = 0
                resultado = 0
                comaAnyadida = False
                operacionGuardada = ""
                Display.Text = ""
            Case "ButtonMenos"

                operacionGuardada = "menos"
                clearSiguiente = True

            Case "ButtonNull"
                MsgBox("ESTE BOTÓN NO HACE NADA")
            Case Else
                If clearSiguiente Then
                    Display.Text = texto
                    clearSiguiente = False
                Else
                    Display.Text += texto
                    calculoGuardado1 = Convert.ToDouble(Display.Text)
                End If

        End Select

    End Sub

End Class
